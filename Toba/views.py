from django.shortcuts import render, redirect, HttpResponse
from django.template.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from .models import Place
from .registers import register_places


def index(request):
    places = Place.objects.all()
    if len(places) == 0:
        register_places()
    return render(request, "toba/index.html", {
        "places": places
    })


def detail(request, place_id):
    place = Place.objects.get(place_id=place_id)
    return render(request, "toba/detail.html", {
        "toba": {"lat": 34.4868, "lng": 136.8431},
        "place": place,
        "place_types": [i_type for i_type in place.type.all()]
    })
