import time
from .models import Place, Type
from .apis import get_places


def register_places2db(results):
    # DBにresultsを登録
    for place in results:
        if "locality" in place["types"] or "political" in place["types"] or "route" in place["types"]:
            continue
        place_keys = place.keys()
        place_obj = Place(place_id=place["place_id"],
                          lat=place["geometry"]["location"]["lat"],
                          lng=place["geometry"]["location"]["lng"],
                          name=place["name"],
                          photo=place["photos"][0]["photo_reference"] if "photos" in place_keys else None,
                          vicinity=place["vicinity"] if "vicinity" in place_keys else None,
                          rating=place["rating"] if "rating" in place_keys else None)
        place_obj.save()

        # ジャンル
        for place_type in place["types"]:
            if not Type.objects.filter(name=place_type).exists():
                type_obj = Type(name=place_type)
                type_obj.save()
            type_obj = Type.objects.get(name=place_type)
            place_obj.type.add(type_obj)


def register_places(radius=50):
    results, next_page_token = get_places(long="136.8431", lat="34.4868", radius=radius, pagetoken=None,
                                          fields="geometry,name,photos,place_id,types")
    register_places2db(results)

    i = 0
    while True:
        i += 1
        results, next_page_token = get_places(long="136.8431", lat="34.4868", radius=radius, pagetoken=None,
                                              fields="geometry,name,photos,place_id,types")
        register_places2db(results)
        print("Registered {} places".format(20 * i))
        time.sleep(2)
        if next_page_token is None:
            break
