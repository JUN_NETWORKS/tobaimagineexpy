import requests
import json

API_KEY = "AIzaSyDfoUdU5HlSaaUEn84zjsR5A822R4o2qeM"
language = "ja"


def get_places(long="136.8431", lat="34.4868", radius=3000, pagetoken=None, fields=None):
    url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" \
          "location={lat},{long}&radius={radius}&fields={fields}&language={lang}&key={KEY}".format(lat=lat, long=long,
                                                                                                   radius=radius,
                                                                                                   fields=fields,
                                                                                                   lang=language,
                                                                                                   KEY=API_KEY)
    url = url + "&pagetoken=" + pagetoken if pagetoken else url

    res = requests.get(url)
    res_json = json.loads(res.text)

    results = res_json["results"]
    if "next_page_token" in res_json.keys():
        next_page_token = res_json["next_page_token"]
    else:
        next_page_token = None

    return results, next_page_token
