from django.db import models


# ジャンル
class Type(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


# places model
class Place(models.Model):
    place_id = models.CharField(max_length=100, primary_key=True)
    lat = models.DecimalField(max_digits=9, decimal_places=6)  # 経度
    lng = models.DecimalField(max_digits=9, decimal_places=6)  # 緯度
    name = models.CharField(max_length=100)  # 施設の名前
    photo = models.CharField(max_length=1000, null=True)  # photo_reference
    vicinity = models.CharField(max_length=1000, null=True)  # 住所
    rating = models.FloatField(null=True)
    type = models.ManyToManyField(Type, blank=True)  # ジャンル

    def __str__(self):
        return self.name
