from django.urls import path
from . import views

app_name = "toba"

urlpatterns = [
    path("", views.index, name="index"),
    path("<str:place_id>", views.detail, name="detail")
]